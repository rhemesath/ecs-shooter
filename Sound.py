import AGameEngine2 as ag

class Sound:
    def __init__(self, soundDir: str):
        self.sounds = ag.loadSoundsFromDir(soundDir)
    
    def play(self, name: str):
        ag.playSound(self.sounds[name])
