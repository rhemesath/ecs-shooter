# ECS-shooter #

A test game project "inspired" by Metroid for the NES written in Python3 and AGameEngine2.
The project's main purpose is to experiment with Entity Component System (ECS) for game development.

### Installation ###

Debian based systems with `apt`:
```shell
foo@bar:~$ sudo apt install python3 sdl2-dev sdl2-image-dev sdl2-mixer-dev sdl2-gfx-dev
```

Arch based systems with `pacman`:
```shell
foo@bar:~$ sudo pacman -S python3 sdl2 sdl2_image sdl2_mixer sdl2_gfx
```

Go into the repository directory and start the game with:
```shell
foo@bar:~/repos/ecs-shooter$ python3 Game.py
```
