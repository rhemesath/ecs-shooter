AGameEngine2 does not have any licenses yet.

AGameEngine2 uses [PySDL2](https://github.com/marcusva/py-sdl2)
which is under the Public Domain.
I did not write this software, however I modified following things:
* only the core `sdl2/` directory is used and placed in this project
* of `sdl2/`, the `test/` and `ext/` directories were removed

Please read the [LICENSE.txt](./sdl2/LICENSE.txt) of PySDL2 for further information.

