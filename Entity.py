from Rect import Rect
from Commander import Commander

class Entity(Rect):
    name: str
    tile = 0
    def __init__(self, game, x=0, y=0, w=0, h=0):
        self.game = game
        self.em = game.em
        self.comps = {}
        self.cmd = Commander()
        Rect.__init__(self, x, y, w, h)
    
    def add(self): self.em.add(self)
    def remove(self): self.em.remove(self)

    def setComp(self, CompClass, *args, **kwargs):
        name = CompClass.name
        if name in self.comps: self.em.removeComp(self.comps[name])
        comp = CompClass(self, *args, **kwargs)
        self.comps[name] = comp
        setattr(self, name, comp)
        self.em.addComp(comp)
    
    def removeComp(self, comp):
        if self.comps.pop(comp.name, None) is None: return
        self.em.removeComp(comp)
        delattr(self, comp.name)

    def __repr__(self) -> str: return f"{self.name}({self.repr()})"
    __str__ = __repr__

class NullEntity(Entity):
    name = "null"
