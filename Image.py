import AGameEngine2 as ag
from Rect import Rect
from Point import Point

class Image(Rect):
    def __init__(self, game, imgPath: str):
        self.game = game
        self.tex = ag.loadTexture(imgPath)
        self.bgColor = ag.getTexturePixelColor(self.tex, 0, 0)

        Rect.__init__(self, 0, 0, ag.getTextureWidth(self.tex), ag.getTextureHeight(self.tex) )
        self.tileSize = self.size // 16
