from Rect import Rect
from Point import Point

class Camera(Rect):
    def __init__(self, game, width: int, height: int):
        self.game = game
        self.realPos = Point(0, 0)
        self.prevTargetPos = Point(0, 0)

        self.hardness = 0.02

        Rect.__init__(self, 0, 0, width, height)

    def update(self, target: Rect):
        dx, dy = (target.pos - self.prevTargetPos).get()
        dy **= 3
        if dy > 0: dy *= 1.2
        elif dy < 0: dy *= 0.45
        self.prevTargetPos.set(*target.pos.get())

        wanted = Point(*target.center) + (dx * 70, dy)
        if dx != 0:
            self.realPos.x = self.__lerp(self.realPos.x, wanted.x, self.hardness)
        self.realPos.y = self.__lerp(self.realPos.y, wanted.y, self.hardness)

        self.pos.set( *(self.realPos - self.size / 2).asInt() )
        self.keepInside(self.game.world)

    def __lerp(self, a: float, b: float, x: float) -> float:
        return (b - a) * x + a
