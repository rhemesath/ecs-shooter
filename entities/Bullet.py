from Entity import Entity
from Rect import Rect
from components import TextureGraphicsComp, MovementComp, PhysicsComp, HealthComp, ColliderComp, SoundComp, ParticleComp

class BulletMovementComp(MovementComp):
    def __init__(self, user, spawner: Rect, dirX: int, off: tuple):
        MovementComp.__init__(self, user)

        self.velX = 5 * dirX

        self.user.center = spawner.center
        self.user.pos += (off[0] * dirX, off[1])

    def update(self):
        self.user.x += self.velX

class BulletHealthComp(HealthComp):
    def __init__(self, *args, **kwargs):
        HealthComp.__init__(self, *args, **kwargs)
        self.cmd.listen("tileColX", lambda: self.cmd.say("explode"))
        self.cmd.listen("explode", self.die)

    def update(self):
        if not self.user.collisionX(self.game.cam): self.die()

class BulletParticleComp(ParticleComp):
    def _setListens(self):
        self.cmd.listen("explode", lambda: self.em.create("explosion", *self.user.center))

class BulletColliderComp(ColliderComp):
    def __init__(self, user, spawner):
        ColliderComp.__init__(self, user)
        self.spawner = spawner

    def onCollision(self, entity):
        if entity is not self.spawner and entity.name != self.user.name: self.cmd.say("explode")

class Bullet(Entity):
    name = "bullet"
    def __init__(self, game, spawner: Rect, dirX: int, off: tuple):
        Entity.__init__(self, game, w=8, h=8)

        self.setComp(BulletMovementComp, spawner, dirX, off)
        self.setComp(PhysicsComp)
        self.setComp(BulletColliderComp, spawner)
        self.setComp(BulletHealthComp)
        self.setComp(BulletParticleComp)
        self.setComp(TextureGraphicsComp, (0, 10 *16, 8, 8) )
        self.setComp(SoundComp, {
            "explode": "bullet"
        })
