from Entity import Entity
from components import AnimationGraphicsComp, ParticleHealthComp, MovementComp

class JumpDustMovementComp(MovementComp):
    def update(self):
        self.user.y -= 0.25

class JumpDust(Entity):
    name = "jumpDust"
    def __init__(self, game, centerX, bottom):
        Entity.__init__(self, game, w=16, h=16)
        self.centerX = centerX
        self.b = bottom

        self.setComp(JumpDustMovementComp)
        self.setComp(ParticleHealthComp)
        self.setComp(AnimationGraphicsComp, (
            ((8  *16, 10 *16, 16, 16), False),
            ((9  *16, 10 *16, 16, 16), False),
            ((10 *16, 10 *16, 16, 16), False),
            ((11 *16, 10 *16, 16, 16), False),
            ((12 *16, 10 *16, 16, 16), False)
        ), 2, loop=False, z=-1)
