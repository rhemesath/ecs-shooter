from Entity import Entity
from components import AnimationGraphicsComp, ParticleHealthComp

class Explosion(Entity):
    name = "explosion"
    def __init__(self, game, x, y):
        Entity.__init__(self, game, w=16, h=16)
        self.center = (x, y)

        self.setComp(ParticleHealthComp)
        self.setComp(AnimationGraphicsComp, (
            ((1 *16, 10 *16, 16, 16), False),
        ), 4, loop=False)
