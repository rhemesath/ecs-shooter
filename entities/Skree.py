from Entity import Entity
from components import AnimationGraphicsComp, ColliderComp, HealthComp, SoundComp, GravityParticleComp, MovementComp, GravityPhysicsComp

class SkreeMovementComp(MovementComp):
    def __init__(self, user):
        MovementComp.__init__(self, user)
        self.velX = 1
        self.follow = False
    
    def update(self):
        player = self.game.player
        if not self.follow and self.user.collisionX(player):
            self.follow = True
            self.user.setComp(GravityPhysicsComp)
            self.cmd.say("follow")

        if self.follow:
            dx = player.x - self.user.x
            if   dx < 0: self.user.x -= self.velX
            elif dx > 0: self.user.x += self.velX

class SkreeColliderComp(ColliderComp):
    def onCollision(self, entity):
        if entity.name in ("bullet", "chargedBullet"): self.cmd.say("explode")

class SkreeHealthComp(HealthComp):
    def __init__(self, user):
        HealthComp.__init__(self, user)
        self.cmd.listen("explode", self.die)
        self.cmd.listen("tileColY", lambda: self.cmd.say("explode") )

class SkreeParticleComp(GravityParticleComp):
    def _setListens(self):
        GravityParticleComp._setListens(self)
        self.cmd.listen("explode", lambda: self.em.create("smoke", self.user.centerX, self.user.b) )

class SkreeGraphicsComp(AnimationGraphicsComp):
    def __init__(self, *args, **kwargs):
        AnimationGraphicsComp.__init__(self, *args, **kwargs)
        self.cmd.listen("follow", self._follow)
    
    def _follow(self): self.frameTime = 4

class Skree(Entity):
    name = "skree"
    tile = 177
    def __init__(self, game, x, y):
        Entity.__init__(self, game, y=y, w=10, h=24)
        self.centerX = x+8

        self.setComp(SkreeMovementComp)
        self.setComp(SkreeParticleComp)
        self.setComp(SkreeColliderComp)
        self.setComp(SkreeHealthComp)
        self.setComp(SkreeGraphicsComp, (
            ((1 *16, 11 *16, 16, 24), False),
            ((2 *16, 11 *16, 8,  24), False),
            ((1 *16, 11 *16, 16, 24), False),
            ((2 *16, 11 *16, 8,  24), True)
        ), 8, centerX=True)
        self.setComp(SoundComp, {
            "explode": "die",
            "follow": "skree"
        })
