from Entity import Entity
from components import TextureGraphicsComp, ColliderComp, MovementComp, PhysicsComp, SoundComp, HealthComp

class RipperMovementComp(MovementComp):
    def __init__(self, user):
        MovementComp.__init__(self, user)
        self.velX = 0.5
        self.dirX = 1

        self.cmd.listen("tileColX", self.turnAround)
    
    def turnAround(self): self.dirX = -self.dirX

    def update(self):
        self.user.x += self.velX * self.dirX

class RipperColliderComp(ColliderComp):
    def onCollision(self, entity):
        if entity.name == "bullet": self.cmd.say("no damage")
        elif entity.name == "chargedBullet": self.cmd.say("explode")

class RipperGraphicsComp(TextureGraphicsComp):
    def __init__(self, *args, **kwargs):
        TextureGraphicsComp.__init__(self, *args, **kwargs)
        self.cmd.listen("tileColX", self.doFlipX)
    
    def doFlipX(self): self.flipX = not self.flipX

class RipperHealthComp(HealthComp):
    def __init__(self, user):
        HealthComp.__init__(self, user)
        self.cmd.listen("explode", self.die)

class Ripper(Entity):
    name = "ripper"
    tile = 179
    def __init__(self, game, x, y):
        Entity.__init__(self, game, x=x, w=16, h=8)
        self.centerY = y + 8

        self.setComp(RipperMovementComp)
        self.setComp(PhysicsComp)
        self.setComp(RipperColliderComp)
        self.setComp(RipperGraphicsComp, (3 *16, 11 *16, 16, 8) )
        self.setComp(RipperHealthComp)
        self.setComp(SoundComp, {
            "no damage": "clink",
            "explode": "die"
        })
