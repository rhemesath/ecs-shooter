from Entity import Entity
from entities.Bullet import BulletColliderComp, BulletHealthComp, BulletMovementComp, BulletParticleComp
from components import PhysicsComp, AnimationGraphicsComp, SoundComp

class ChargedBulletParticleComp(BulletParticleComp):
    def _setListens(self):
        BulletParticleComp._setListens(self)
        self.cmd.listen("explode", self.makePebbles)
    
    def makePebbles(self):
        for i in range(3):
            self.em.create("pebble", self.user.centerX, self.user.b)

class ChargedBullet(Entity):
    name = "chargedBullet"
    def __init__(self, game, spawner: Entity, dirX: int, off:tuple=(0, 0) ):
        Entity.__init__(self, game, w=16, h=16)

        self.setComp(BulletMovementComp, spawner, dirX, off)
        self.setComp(PhysicsComp)
        self.setComp(BulletColliderComp, spawner)
        self.setComp(ChargedBulletParticleComp)
        self.setComp(BulletHealthComp)
        self.setComp(AnimationGraphicsComp, (
            ((13 *16, 11 *16, 16, 16), False),
            ((14 *16, 11 *16, 16, 16), False)
        ), 3)
        self.setComp(SoundComp, {
            "explode": "strongExplosion"
        })
