from Entity import Entity
from components import GravityPhysicsComp, HealthComp, TextureGraphicsComp, MovementComp
from random import random, randint

class PebblePhysicsComp(GravityPhysicsComp):
    def __init__(self, user):
        GravityPhysicsComp.__init__(self, user)
        maxX = 1.5
        self.vel.set((random()-0.5)*2*maxX, random()*-3 -0.2 )
        self.maxVel.x = maxX
    
    def _onTileColX(self):
        self.vel.x *= -0.75
    
    def _onTileColY(self):
        self.vel.y *= -0.65

class PebbleHealthComp(HealthComp):
    def __init__(self, user, bounces: int):
        HealthComp.__init__(self, user)
        self.numBounces = 0
        self.maxBounces = bounces
        self.cmd.listen("tileColY", self.bounce)

    def bounce(self):
        self.numBounces += 1
        if self.numBounces == self.maxBounces: self.die()

class PebbleGraphicsComp(TextureGraphicsComp):
    def __init__(self, *args, **kwargs):
        TextureGraphicsComp.__init__(self, *args, **kwargs)
        self.angleVel = (random()*2-1)*5
        self.angle = random() * 360

    def update(self):
        TextureGraphicsComp.update(self)
        self.angle += self.angleVel

class Pebble(Entity):
    name = "pebble"
    def __init__(self, game, x, y):
        Entity.__init__(self, game, w=8, h=8)
        self.center = (x, y)

        self.setComp(PebblePhysicsComp)
        self.setComp(PebbleHealthComp, 1+randint(0, 1) )
        self.setComp(PebbleGraphicsComp, (0, 10*16+8, 8, 8) )
