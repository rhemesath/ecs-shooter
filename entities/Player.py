import AGameEngine2 as ag
from Entity import Entity
from components import MovementComp, GraphicsSelectComp, TextureGraphicsComp, AnimationGraphicsComp, GravityPhysicsComp, JumpComp, HealthComp, ShootComp, ColliderComp, SoundComp, GravityParticleComp

class PlayerMovementComp(MovementComp):
    def __init__(self, user):
        MovementComp.__init__(self, user)

        self.keyLeft  = "a"
        self.keyRight = "d"
        self.keyUp    = "w"
        self.keyDown  = "s"
        self.keyA     = "k"
        self.keyB     = "j"
        self.canPressA = True
        self.canPressB = True

        self.accX = 0.15
        self.prevAccX = 0
        self.slowX = 0.75
        self.walkVel = 2
    
    def update(self):
        dx = 0
        if ag.keyPressed(self.keyLeft):  dx -= 1
        if ag.keyPressed(self.keyRight): dx += 1

        physics = self.user.physics
        physics.maxVel.x = self.walkVel
        self.prevAccX = physics.acc.x
        if dx == -1:
            physics.acc.x = -self.accX
            self.cmd.say("aimLeft")
            self.cmd.say("run")
        elif dx == 1:
            physics.acc.x = self.accX
            self.cmd.say("aimRight")
            self.cmd.say("run")
        else:
            physics.acc.x = 0
            physics.vel.x *= self.slowX
        if physics.acc.x - self.prevAccX != 0: self.cmd.say("didTurn")

        if ag.keyPressed(self.keyA):
            if self.canPressA: self.cmd.say("jump")
            self.canPressA = False
        else:
            self.canPressA = True
            self.cmd.say("cancelJump")

        if ag.keyPressed(self.keyB):
            if self.canPressB: self.cmd.say("shoot")
            else: self.cmd.say("charge")
            self.canPressB = False
        else:
            if not self.canPressB: self.cmd.say("chargeRelease")
            self.canPressB = True

        self.user.keepInsideX(self.game.world)

class PlayerColliderComp(ColliderComp):
    def onCollision(self, entity):
        if entity.name in ("zoomer", "skree", "ripper"): self.cmd.say("die")

class PlayerGraphicsComp(GraphicsSelectComp):
    def _setListens(self):
        self.cmd.listen("aimLeft", self.flipLeft)
        self.cmd.listen("aimRight", self.flipRight)
        self.cmd.listen("stand", lambda: self.status.add("stand"))
        self.cmd.listen("run", lambda: self.status.add("run"))

    def _setUsed(self):
        if self.user.jump.onGround:
            if "run" in self.status:
                self.use("run")
                slow = 0.3
                self.current.frameTime = slow + (1 - slow) * (abs(self.user.physics.vel.x) / self.user.physics.maxVel.x)
            else: self.use("stand")
        else:
            self.use("jump")

    def render(self):
        self.current.flipX = self.flipX
        GraphicsSelectComp.render(self)

class PlayerParticleComp(GravityParticleComp):
    def _setListens(self):
        GravityParticleComp._setListens(self)
        self.cmd.listen("didTurn", self._makeDustSide)
        self.cmd.listen("didJump", lambda: self.em.create("jumpDust", self.user.centerX, self.user.b) )
        self.cmd.listen("chargeParticle", self._chargeParticle)
        self.cmd.listen("chargeRelease", self._chargeParticleDone)

        self._charge = None

    def _makeDustSide(self):
        if not self.user.jump.onGround: return
        accX = self.user.physics.acc.x
        if accX == 0: return
        deltaAccX = accX - self.user.move.prevAccX
        self.em.create("dust", self.user, 1 if deltaAccX < 0 else -1)

    def _chargeParticle(self):
        if self._charge is None:
            self._charge = self.em.create("chargeParticle", self.user, self.user.graphics.flipX, self.user.shoot.off)
    
    def _chargeParticleDone(self):
        self._charge = None

class PlayerShootComp(ShootComp):
    def __init__(self, *args, **kwargs):
        ShootComp.__init__(self, *args, **kwargs)
        self.chargeStartWaitFrames = 30
        self.chargeStartWait = self.chargeStartWaitFrames
        self.chargeWaitFrames = 60
        self.chargeWait = self.chargeWaitFrames
        self.soundWaitFrames = 4
        self.soundWait = 0

        self.cmd.listen("charge", self.charge)
        self.cmd.listen("chargeRelease", self.chargeRelease)
        self.cmd.listen("shootCharged", self.shootCharged)

    def shootCharged(self):
        self.em.create("chargedBullet", self.user, self.dirX, self.off)

    def charge(self):
        if self.chargeStartWait > 0:
            self.chargeStartWait -= 1
            return

        if self.chargeWait > 0:
            if self.chargeWait == self.chargeWaitFrames: self.cmd.say("chargeStart")
            self.cmd.say("chargeParticle")
            self.chargeWait -= 1
            return

        if self.soundWait > 0: self.soundWait -= 1
        else:
            self.cmd.say("chargeReadySound")
            self.soundWait = self.soundWaitFrames

    def chargeRelease(self):
        if self.chargeWait == 0:
            self.cmd.say("shootCharged")
        self.chargeStartWait = self.chargeStartWaitFrames
        self.chargeWait = self.chargeWaitFrames
        self.soundWait = 0

class Player(Entity):
    name = "player"
    tile = 225
    def __init__(self, game, x, y):
        Entity.__init__(self, game, x, y, 16, 32)
        self.game.player = self

        self.setComp(JumpComp, 6)
        self.setComp(PlayerMovementComp)
        self.setComp(GravityPhysicsComp)
        self.setComp(PlayerShootComp, "bullet", 0, (12, -5) )
        self.setComp(PlayerColliderComp)
        self.setComp(HealthComp)
        self.setComp(PlayerParticleComp)
        self.setComp(PlayerGraphicsComp, "stand", {
            "stand": TextureGraphicsComp(self, (0, 14 *16, 32, 32), centerX=True, floor=True),
            "jump": TextureGraphicsComp(self, (8 *16, 14 *16, 32, 32), centerX=True, floor=True),
            "run": AnimationGraphicsComp(self, (
                ((2 *16, 14 *16, 32, 32), None),
                ((4 *16, 14 *16, 32, 32), None),
                ((6 *16, 14 *16, 32, 32), None)
            ), 3, centerX=True, floor=True)
        })
        self.setComp(SoundComp, {
            "didShoot": "shoot",
            "didJump": "jump",
            "didLand": "land",
            "chargeStart": "charge",
            "chargeReadySound": "chargeHold",
            "shootCharged": "chargeShoot"
        })
