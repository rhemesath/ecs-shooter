from Entity import Entity
from components import AnimationGraphicsComp, GravityPhysicsComp, MovementComp, HealthComp, ColliderComp, SoundComp, GravityParticleComp

class ZoomerMovementComp(MovementComp):
    def __init__(self, user):
        MovementComp.__init__(self, user)
        
        self.velX = 1
        self.dirX = 1

        self.cmd.listen("tileColX", self.turnAround)
    
    def update(self):
        self.user.x += self.velX * self.dirX

        world = self.game.world
        if self.user.l < 0 or self.user.r > world.r:
            self.user.keepInsideX(world)
            self.turnAround()
    
    def turnAround(self): self.dirX = -self.dirX

class ZoomerColliderComp(ColliderComp):
    def onCollision(self, entity):
        if entity.name in ("bullet", "chargedBullet"): self.cmd.say("explode")

class ZoomerHealthComp(HealthComp):
    def __init__(self, *args, **kwargs):
        HealthComp.__init__(self, *args, **kwargs)
        self.cmd.listen("explode", self.die)

class ZoomerParticleComp(GravityParticleComp):
    def _setListens(self):
        GravityParticleComp._setListens(self)
        self.cmd.listen("explode", lambda: self.em.create("smoke", self.user.centerX, self.user.b))

class Zoomer(Entity):
    name = "zoomer"
    tile = 176
    def __init__(self, game, x, y):
        Entity.__init__(self, game, x, y, 16, 16)

        self.setComp(ZoomerMovementComp)
        self.setComp(GravityPhysicsComp)
        self.setComp(ZoomerColliderComp)
        self.setComp(ZoomerHealthComp)
        self.setComp(ZoomerParticleComp)
        self.setComp(AnimationGraphicsComp, (
            ((0, 11 *16, 16, 16), False),
            ((0, 11 *16, 16, 16), True)
        ), 7)
        self.setComp(SoundComp, {
            "explode": "die"
        })
