from Entity import Entity
from components import ParticleHealthComp, AnimationGraphicsComp, MovementComp

class DustMovementComp(MovementComp):
    def __init__(self, user, rect, dirX: int, move: bool):
        MovementComp.__init__(self, user)

        user.b = rect.b
        flip = dirX < 0

        if move:
            if flip: user.r = rect.l
            else: user.l = rect.r
            self.vel = dirX * 0.5
        else:
            if flip: user.r = rect.centerX
            else: user.l = rect.centerX
            self.vel = 0

    def update(self):
        self.user.x += self.vel

class Dust(Entity):
    name = "dust"
    def __init__(self, game, rect, dirX: int, move:bool=True):
        Entity.__init__(self, game, w=16, h=16)
        
        flip = dirX < 0
        self.setComp(DustMovementComp, rect, dirX, move)
        self.setComp(ParticleHealthComp)
        self.setComp(AnimationGraphicsComp, (
            ((5 *16, 10 *16, 16, 16), flip),
            ((6 *16, 10 *16, 16, 16), flip),
            ((7 *16, 10 *16, 16, 16), flip)
        ), 4, loop=False, z=-1)
