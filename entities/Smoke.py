from Entity import Entity
from components import ParticleHealthComp, AnimationGraphicsComp

class Smoke(Entity):
    name = "smoke"
    def __init__(self, game, centerX, bottom):
        Entity.__init__(self, game, w=16, h=16)
        self.centerX = centerX
        self.b = bottom

        self.setComp(ParticleHealthComp)
        self.setComp(AnimationGraphicsComp, (
            ((2 *16, 10 *16, 16, 16), False),
            ((3 *16, 10 *16, 16, 16), False),
            ((4 *16, 10 *16, 16, 16), False)
        ), 4, loop=False)
