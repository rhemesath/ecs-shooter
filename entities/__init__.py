from pathlib import Path
from importlib import import_module

entityClasses = {}
for path in Path(__file__).parent.iterdir():
    className = path.stem
    if not str(path).endswith(".py") or className == "__init__": continue
    EntityClass = getattr(import_module(f"entities.{className}"), className)
    entityClasses[EntityClass.name] = EntityClass
    if EntityClass.tile != 0: entityClasses[EntityClass.tile] = EntityClass
