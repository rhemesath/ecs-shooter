from Entity import Entity
from Point import Point
from components import HealthComp, AnimationGraphicsComp, GraphicsSelectComp, MovementComp

class ChargeParticleMovementComp(MovementComp):
    def __init__(self, user, spawner, left: bool, off: tuple):
        MovementComp.__init__(self, user)
        self.spawner = spawner
        self.dirX = -1 if left else 1
        self.off = Point(*off)

        spawner.cmd.listen("aimLeft", self.aimLeft)
        spawner.cmd.listen("aimRight", self.aimRight)
        self.cmd.listen("die", self.unlisten)
        self.update()

    def update(self):
        self.user.center = self.spawner.center
        self.user.pos += self.off * (self.dirX, 1)
    
    def aimLeft(self): self.dirX = -1
    def aimRight(self): self.dirX = 1

    def unlisten(self):
        self.spawner.cmd.unlisten("aimLeft", self.aimLeft)
        self.spawner.cmd.unlisten("aimRight", self.aimRight)

class ChargeParticleHealthComp(HealthComp):
    def __init__(self, user, spawner):
        HealthComp.__init__(self, user)
        self.spawner = spawner
        self.spawner.cmd.listen("chargeRelease", self.sayDie)
        self.cmd.listen("die", self.unlisten)
    
    def sayDie(self):
        self.cmd.say("die")

    def unlisten(self):
        self.spawner.cmd.unlisten("chargeRelease", self.sayDie)

class ChargeParticleGraphicsComp(GraphicsSelectComp):
    def __init__(self, user, spawner, *args, **kwargs):
        self.spawner = spawner
        GraphicsSelectComp.__init__(self, user, *args, **kwargs)
        self.charge = True

    def switch(self):
        self.charge = False

    def unlisten(self):
        self.spawner.cmd.unlisten("chargeReadySound", self.switch)

    def _setListens(self):
        self.cmd.listen("die", self.unlisten)
        self.spawner.cmd.listen("chargeReadySound", self.switch)
    
    def _setUsed(self):
        if self.charge: self.use("charge")
        else: self.use("hold")

class ChargeParticle(Entity):
    name = "chargeParticle"
    def __init__(self, game, spawner, left: bool, off:tuple=(0, 0) ):
        Entity.__init__(self, game, w=16, h=16)

        self.setComp(ChargeParticleMovementComp, spawner, left, off)
        self.setComp(ChargeParticleHealthComp, spawner)
        self.setComp(ChargeParticleGraphicsComp, spawner, "charge", {
            "charge": AnimationGraphicsComp(self, (
                ((13 *16, 10 *16, 16, 16), False),
                ((14 *16, 10 *16, 16, 16), False)
            ), 2),
            "hold": AnimationGraphicsComp(self, (
                ((15 *16, 10 *16, 16, 16), False),
                ((15 *16, 11 *16, 16, 16), False)
            ), 3)
        })
