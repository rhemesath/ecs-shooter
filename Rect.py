import AGameEngine2 as ag
from Point import Point

class Rect:
    def __init__(self, x, y, w, h):
        self.pos  = Point(x, y)
        self.size = Point(w, h)
    
    def get(self) -> tuple: return (*self.pos.get(), *self.size.get())
    def set(self, x, y, w, h):
        self.pos.set(x, y)
        self.size.set(w, h)

    def asInt(self) -> tuple: return (*self.pos.asInt(), *self.size.asInt())

    def getTile(self, *args) -> int:
        if len(args) == 2:
            a, delta = args
            b = a
        else: a, b, delta = args
        if delta < 0: return int(a // 16)
        else: return int((b - 1) // 16)

    def keepInside(self, rect):
        self.keepInsideX(rect)
        self.keepInsideY(rect)
    def keepInsideX(self, rect):
        if   self.l < rect.l: self.l = rect.l
        elif self.r > rect.r: self.r = rect.r
    def keepInsideY(self, rect):
        if   self.t < rect.t: self.t = rect.t
        elif self.b > rect.b: self.b = rect.b

    def collision(self, rect) -> bool:
        return self.collisionX(rect) and self.collisionY(rect)
    def collisionX(self, rect) -> bool:
        return self.l < rect.r and self.r > rect.l
    def collisionY(self, rect) -> bool:
        return self.t < rect.b and self.b > rect.t

    @property
    def x(self): return self.pos.x
    @property
    def y(self): return self.pos.y
    @property
    def w(self): return self.size.x
    @property
    def h(self): return self.size.y
    @property
    def r(self): return self.pos.x + self.size.x
    @property
    def b(self): return self.pos.y + self.size.y
    @property
    def center(self) -> tuple: return (self.pos + self.size / 2).get()
    @property
    def centerX(self): return self.pos.x + self.size.x / 2
    @property
    def centerY(self): return self.pos.y + self.size.y / 2

    @x.setter
    def x(self, x): self.pos.x = x
    @y.setter
    def y(self, y): self.pos.y = y
    @w.setter
    def w(self, w): self.size.x = w
    @h.setter
    def h(self, h): self.size.y = h
    @r.setter
    def r(self, r): self.pos.x = r - self.size.x
    @b.setter
    def b(self, b): self.pos.y = b - self.size.y
    @center.setter
    def center(self, center: tuple): self.pos = Point(*center) - self.size / 2
    @centerX.setter
    def centerX(self, x): self.pos.x = x - self.size.x / 2
    @centerY.setter
    def centerY(self, y): self.pos.y = y - self.size.y / 2

    l, t = x, y

    def repr(self) -> str: return f"{self.pos.x}, {self.pos.y}, {self.size.x}, {self.size.y}"
    def __repr__(self) -> str: return f"Rect({self.repr()})"
    def __str__(self) -> str: return f"<{self.repr()}>"

    def render(self, tex, color, replace:bool=False):
        ag.drawRect(tex, color, int(self.pos.x), int(self.pos.y), int(self.size.x), int(self.size.y), replace)
