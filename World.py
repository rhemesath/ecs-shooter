import AGameEngine2 as ag
import worldData
from Rect import Rect
from Point import Point

class World(Rect):
    def __init__(self, game):
        self.game = game
        self.em = game.em

        self.colMap = worldData.collisionLayer
        self.map = worldData.layers[0]

        self.tileSize = Point(len(self.map[0]), len(self.map))
        Rect.__init__(self, 0, 0, self.tileSize.w * 16, self.tileSize.h * 16)

    def createEntities(self):
        for y, line in enumerate(self.map):
            posY = y * 16
            for x, tile in enumerate(line):
                if tile == 0: continue
                if self.em.create(tile, x*16, posY) is None: continue
                line[x] = 0

    def exists(self, x: int, y: int) -> bool:
        return x >= 0 and x < self.tileSize.w and y >= 0 and y < self.tileSize.h
    
    def isColTile(self, x: int, y: int) -> int: # (0 or 1)
        if self.exists(x, y): return self.colMap[y][x]
        return 0

    def render(self):
        cam = self.game.cam
        img  = self.game.img
        imgTex = img.tex
        imgTileW = img.tileSize.w
        tex = self.game.tex
        tileW = self.tileSize.w

        ag.fillTexture(tex, img.bgColor)
        rangeX = range(cam.getTile(cam.l, -1), cam.getTile(cam.r, -1) + 1)
        for y in range(cam.getTile(cam.t, -1), cam.getTile(cam.b, -1) + 1):
            if y < 0 or y >= self.tileSize.h: continue
            line = self.map[y]
            tileY = y * 16 - cam.y
            for x in rangeX:
                if x < 0 or x >= tileW: continue
                i = line[x]
                if i == 0: continue
                ag.drawTexture(tex, imgTex, x*16 - cam.x, tileY,
                    ((i % imgTileW)*16, (i // imgTileW)*16, 16, 16)
                )
