
class Commander:
    def __init__(self):
        self.listeners = {}

    def say(self, word: str):
        for action in self._getActionList(word): action()

    def listen(self, word: str, action: callable):
        self._getActionList(word).append(action)

    def unlisten(self, word: str, action: callable):
        l = self._getActionList(word)
        if action not in l: return
        l.remove(action)

    def _getActionList(self, word: str) -> list:
        l = self.listeners.get(word)
        if l is None:
            l = []
            self.listeners[word] = l
        return l
