import AGameEngine2 as ag
from Rect import Rect
from Point import Point

class Component:
    name: str
    def __init__(self, user):
        self.user = user
        self.game = user.game
        self.cmd  = user.cmd
        self.em   = user.em
    
    def update(self): pass

class NullComp(Component):
    name = "null"

class MovementComp(Component):
    name = "move"

class GraphicsComp(Component):
    name = "graphics"
    def __init__(self, user, z=0):
        Component.__init__(self, user)
        self.dest = Rect(0, 0, 0, 0)
        self.z = z

    def render(self): pass

class RectGraphicsComp(GraphicsComp):
    def __init__(self, user, color, z=0):
        GraphicsComp.__init__(self, user, z)
        self.color = color
    
    def update(self):
        self.dest.set(*self.user.asInt())

    def render(self):
        self.dest.pos -= self.game.cam.pos
        Rect.render(self.dest, self.game.tex, self.color)

class TextureGraphicsComp(GraphicsComp):
    def __init__(self, user, src: tuple, centerX:bool=False, floor:bool=False, flipX:bool=False, z=0):
        GraphicsComp.__init__(self, user, z)
        self.img = self.game.img
        self.src = Rect(0, 0, 0, 0)

        self.centerX = centerX
        self.floor = floor
        self.angle = 0.0
        
        self.setSrc(src, flipX)

    def setSrc(self, src: tuple, flipX: bool):
        self.src.set(*src)
        self.dest.size.set(*self.src.size.get())
        if flipX is not None: self.flipX = flipX

    def update(self):
        if self.centerX: self.dest.pos.x = self.user.x + (self.user.w - self.dest.w) / 2
        else: self.dest.pos.x = self.user.x
        
        if self.floor: self.dest.b = self.user.b
        else: self.dest.pos.y = self.user.y
        
        self.dest.pos.set(*self.dest.pos.asInt())
    
    def render(self):
        self.dest.pos -= self.game.cam.pos
        ag.drawTextureMod(self.game.tex, self.img.tex, *self.dest.pos.get(), self.src.get(), flipX=self.flipX, angle=self.angle)

class AnimationGraphicsComp(TextureGraphicsComp):
    def __init__(self, user, frames: tuple, frameWait: int, centerX:bool=False, floor:bool=False, loop:bool=True, z=0):
        TextureGraphicsComp.__init__(self, user, frames[0][0], centerX=centerX, floor=floor, z=z)
        self.frames = frames
        self.frameWait = frameWait
        self.numFrames = len(frames)
        self.frameTime = 1
        self.reset()

        if loop: self.cmd.listen("animationDone", self.reset)

    def reset(self):
        self.frameIndex = 0
        self.counter = 0
        self.setSrc(*self.frames[self.frameIndex])

    def update(self):
        if self.counter >= self.frameWait:
            self.frameIndex += 1
            if self.frameIndex == self.numFrames:
                self.cmd.say("animationDone")
            else:
                self.counter = 0
                self.setSrc(*self.frames[self.frameIndex])
        else: self.counter += self.frameTime
        TextureGraphicsComp.update(self)

class GraphicsSelectComp(GraphicsComp):
    def __init__(self, user, use: str, graphics: dict):
        GraphicsComp.__init__(self, user)

        self.graphics = graphics
        self.current = graphics[use]
        self.flipX = False
        self.status = set()

        self._setListens()
    
    def _setListens(self): pass
    def _setUsed(self): pass

    def flipLeft(self):  self.flipX = True
    def flipRight(self): self.flipX = False

    def use(self, name: str):
        new = self.graphics[name]
        if type(new) is AnimationGraphicsComp and new is not self.current: new.reset()
        self.current = new
        self.z = new.z

    def update(self):
        self._setUsed()
        self.current.update()
        self.dest = self.current.dest
        self.status.clear()
    
    def render(self):
        self.current.render()

class PhysicsComp(Component, Rect):
    name = "physics"
    def __init__(self, user):
        Component.__init__(self, user)
        Rect.__init__(self, *user.get() ) # prev pos of user
        self.world = self.game.world
        self.delta = Point(0, 0)
    
    def update(self):
        self._checkTileCols(self.user)
        self.set(*self.user.get())

    def _checkTileCols(self, rect):
        currentY = rect.y
        rect.y = self.y
        self._checkTileColX(rect)
        rect.y = currentY
        self._checkTileColY(rect)
    
    def _checkTileColX(self, rect):
        dx = rect.x - self.x
        self.delta.x = dx
        if dx == 0: return
        tile = Point(rect.getTile(rect.l, rect.r, dx), 0)

        for tile.y in range(rect.getTile(rect.t, -1), rect.getTile(rect.b, 1) + 1):
            if not self.world.isColTile(tile.x, tile.y): continue
            if dx < 0: rect.l = (tile.x + 1) * 16
            else: rect.r = tile.x * 16
            self.cmd.say("tileColX")
            break
    
    def _checkTileColY(self, rect):
        dy = rect.y - self.y
        self.delta.y = dy
        if dy == 0: return
        tile = Point(0, rect.getTile(rect.t, rect.b, dy) )

        for tile.x in range(rect.getTile(rect.l, -1), rect.getTile(rect.r, 1) + 1):
            if not self.world.isColTile(tile.x, tile.y): continue
            if dy < 0: rect.t = (tile.y + 1) * 16
            else: rect.b = tile.y * 16
            self.cmd.say("tileColY")
            break

class GravityPhysicsComp(PhysicsComp):
    def __init__(self, user):
        PhysicsComp.__init__(self, user)

        self.vel = Point(0, 0)
        self.acc = Point(0, 0.2)
        self.maxVel = Point(0, 15)

        self.cmd.listen("tileColY", self._onTileColY)
        self.cmd.listen("tileColX", self._onTileColX)

    def update(self):
        self.vel += self.acc
        self.vel.limit(*self.maxVel.get())
        self.user.pos += self.vel

        PhysicsComp.update(self)

    def _onTileColY(self):
        if self.vel.y > 1.5: self.cmd.say("didLand")
        self.vel.y = 0
    
    def _onTileColX(self):
        self.vel.x = 0

class JumpComp(Component):
    name = "jump"
    def __init__(self, user, height):
        Component.__init__(self, user)

        self.height = height
        self.cancelFactor = 0.55

        self.onGround = False
        self.canCancel = False

        self.cmd.listen("jump", self.jump)
        self.cmd.listen("cancelJump", self.cancel)
    
    def jump(self):
        if not self.onGround: return
        self.user.physics.vel.y = -self.height
        self.onGround = False
        self.canCancel = True
        self.cmd.say("didJump")
    
    def cancel(self):
        if not self.canCancel or self.user.physics.vel.y >= 0: return
        self.user.physics.vel.y *= self.cancelFactor
        self.canCancel = False

    def update(self):
        self.onGround = self._isOnGround(self.user)

    def _isOnGround(self, rect) -> bool:
        tile = Point(0, rect.getTile(rect.b, -1) )

        for tile.x in range(rect.getTile(rect.l, -1), rect.getTile(rect.r, 1) + 1):
            if self.game.world.isColTile(tile.x, tile.y): return True
        return False

class HealthComp(Component):
    name = "health"
    def __init__(self, user):
        Component.__init__(self, user)
        self.cmd.listen("die", self.die)

    def die(self):
        self.user.remove()

    def update(self):
        if self.user.t > self.game.world.b:
            self.cmd.say("die")

class ParticleHealthComp(HealthComp):
    def __init__(self, user):
        HealthComp.__init__(self, user)
        self.cmd.listen("animationDone", self.die)

class ShootComp(Component):
    name = "shoot"
    def __init__(self, user, entityName: str, coolDown, off=(0, 0) ):
        Component.__init__(self, user)

        self.entityName = entityName
        self.wait = 0
        self.coolDown = coolDown
        self.dirX = 1
        self.off = off
    
        self.cmd.listen("shoot", self.shoot)
        self.cmd.listen("aimLeft", self.aimLeft)
        self.cmd.listen("aimRight", self.aimRight)

    def aimLeft(self):  self.dirX = -1
    def aimRight(self): self.dirX =  1

    def shoot(self):
        if self.wait > 0: return
        self.em.create(self.entityName, self.user, self.dirX, self.off)
        self.wait = self.coolDown
        self.cmd.say("didShoot")

    def update(self):
        if self.wait > 0: self.wait -= 1

class ColliderComp(Component):
    name = "collider"
    def onCollision(self, entity): pass

class _PlaySoundCmd:
    def __init__(self, snd, sound_name: str):
        self.name = sound_name
        self.play = snd.play
    def __call__(self): self.play(self.name)

class SoundComp(Component):
    name = "sound"
    def __init__(self, user, mapping: dict):
        Component.__init__(self, user)
        snd = self.game.snd

        for word, sound_name in mapping.items():
            self.cmd.listen(word, _PlaySoundCmd(snd, sound_name) )

class ParticleComp(Component):
    name = "particle"
    def __init__(self, user):
        Component.__init__(self, user)
        self._setListens()
    
    def _setListens(self): pass

class GravityParticleComp(ParticleComp):
    def _setListens(self):
        self.cmd.listen("didLand", self._landingDust)

    def _landingDust(self):
        for dirX in (-1, 1):
            self.em.create("dust", self.user, dirX, move=False)
