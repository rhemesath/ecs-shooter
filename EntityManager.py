from Entity import Entity, NullEntity
from components import Component, NullComp
from Rect import Rect

from entities import entityClasses

class EntityManager:
    def __init__(self, game):
        self.game = game

        updatePadding = 4 # 4 tiles around the camera where entities are updated
        self.updateRect = Rect(0, 0, game.cam.w + 2*updatePadding*16, game.cam.h + 2*updatePadding*16)
        self.doUpdate = set()

        self.nullEntity = NullEntity(game)
        self.nullComp = NullComp(self.nullEntity)
        self.entities = []
        self.comps = {}
        self.compRemoveQ = set()

    def create(self, key, *args, **kwargs) -> Entity:
        EntityClass = entityClasses.get(key)
        if EntityClass is None: return None
        entity = EntityClass(self.game, *args, **kwargs)
        self.add(entity)
        return entity

    def add(self, entity: Entity):
        if entity in self.entities: return
        self.entities.append(entity)

    def remove(self, entity: Entity):
        if entity not in self.entities: return
        self.entities[self.entities.index(entity)] = self.nullEntity
        for comp in entity.comps.values():
            self.removeComp(comp)

    def addComp(self, comp: Component):
        compList = self.__getCompList(comp.name)
        if comp in compList: return
        compList.append(comp)

    def removeComp(self, comp: Component):
        compList = self.__getCompList(comp.name)
        if comp not in compList: return
        compList[compList.index(comp)] = self.nullComp
        self.compRemoveQ.add(comp.name)

    def __getCompList(self, name: str) -> list:
        l = self.comps.get(name)
        if l is None:
            l = []
            self.comps[name] = l
        return l

    def update(self):
        # check if entities need to be updated
        self.updateRect.center = self.game.cam.center
        self.doUpdate = set(entity for entity in self.entities
            if entity.collision(self.updateRect)
        )

        for name, compList in self.comps.items():
            comps = [comp for comp in compList
                if comp.user in self.doUpdate
            ]
            if name == "collider":
                self.__updateCollisions(comps)
                continue
            for comp in comps:
                comp.update()
        
        while self.nullEntity in self.entities:
            self.entities.remove(self.nullEntity)
        
        for name in self.compRemoveQ:
            compList = self.comps[name]
            while self.nullComp in compList:
                compList.remove(self.nullComp)
        self.compRemoveQ.clear()

    def __updateCollisions(self, colliderComps: list):
        last = len(colliderComps)
        for subjIndex in range(last - 1):
            subjCollider = colliderComps[subjIndex]
            subj = subjCollider.user
            for objIndex in range(subjIndex + 1, last):
                objCollider = colliderComps[objIndex]
                obj = objCollider.user

                if not subj.collision(obj): continue
                subjCollider.onCollision(obj)
                objCollider.onCollision(subj)

    def render(self):
        for comp in sorted(self.__getCompList("graphics"), key=lambda comp: comp.z):
            if comp.user in self.doUpdate: comp.render()
