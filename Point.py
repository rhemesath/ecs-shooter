
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def get(self) -> tuple: return (self.x, self.y)
    def set(self, x, y):
        self.x = x
        self.y = y
    
    def asInt(self) -> tuple: return (int(self.x), int(self.y))

    def limit(self, x, y):
        self.x = self.__limit(self.x, x)
        self.y = self.__limit(self.y, y)
    def __limit(self, a, lim):
        if   a >  lim: return  lim
        elif a < -lim: return -lim
        return a

    @property
    def w(self): return self.x
    @property
    def h(self): return self.y

    @w.setter
    def w(self, w): self.x = w
    @h.setter
    def h(self, h): self.y = h

    @staticmethod
    def getXY(v) -> tuple:
        t = type(v)
        if t is Point: return v.get()
        elif t is tuple: return v
        else: return (v, v)

    def __add__(self, v):
        x, y = self.getXY(v)
        return Point(self.x + x, self.y + y)
    def __sub__(self, v):
        x, y = self.getXY(v)
        return Point(self.x - x, self.y - y)
    def __truediv__(self, v):
        x, y = self.getXY(v)
        return Point(self.x / x, self.y / y)
    def __floordiv__(self, v):
        x, y = self.getXY(v)
        return Point(self.x // x, self.y // y)
    def __mul__(self, v):
        x, y = self.getXY(v)
        return Point(self.x * x, self.y * y)

    def repr(self) -> str: return f"{self.x}, {self.y}"
    def __repr__(self) -> str: return f"Point({self.repr()})"
    def __str__(self) -> str: return f"<{self.repr()}>"
