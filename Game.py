import AGameEngine2 as ag

from Image import Image
from Sound import Sound
from World import World
from EntityManager import EntityManager
from Camera import Camera

class Game:
    def __init__(self):
        self.title  = "Pattern Game"
        self.scale  = 3
        self.width  = 16 * 16
        self.height = 12 * 16
        self.fps = 60
        self.fullscreen = False

        ag.setFPS(self.fps)
        ag.createWindow(self.title, self.width, self.height)
        ag.setWindowScale(self.scale, self.scale)
        if self.fullscreen:
            self.fullscreen = False
            self.toggleFullscreen()
        self.tex = ag.getWindowTexture()

        # TEMP
        s = 30 *16
        self.hs = s // 2
        self.lightTex = ag.createTexture(s, s)
        tempTex = ag.createTexture(self.hs, self.hs)
        for py in range(self.hs):
            y = py - self.hs
            for px in range(self.hs):
                x = px - self.hs
                ag.drawPixel(tempTex, (0, 0, 0, int(
                    min(1.0, (x*x + y*y)**0.5 / self.hs) * 255
                )), px, py, replace=True)
        for flipX in (0, 1):
            for flipY in (0, 1):
                ag.drawTextureMod(self.lightTex, tempTex, flipX * self.hs, flipY * self.hs, flipX=bool(flipX), flipY=bool(flipY), replace=True)
        self.fxTex = ag.createTexture(self.width, self.height)
        # END TEMP

        self.run()

    def run(self):
        self.img = Image(self, "resources/images/images.png")
        self.snd = Sound("resources/sounds")
        music = ag.loadMusic("resources/music/background.mp3")
        ag.playMusic(music, loops=-1)
        ag.setMusicPos(40)

        self.cam = Camera(self, self.width, self.height)
        self.em = None
        self.em = EntityManager(self)
        self.world = World(self)

        self.player = None
        self.world.createEntities()

        self.__running = True
        while self.__running: self.__update()
    
    def __update(self):
        if ag.shouldWindowClose() or ag.wasKeyPressed("escape"):
            self.quit()
            return
        if ag.wasKeyPressed("f11"): self.toggleFullscreen()
        self.time = ag.getTime()

        self.em.update()
        self.cam.update(self.player.graphics.dest)

        self.world.render()
        self.em.render()

        # TEMP
        ag.fillTexture(self.fxTex, (0, 0, 0, 255) )
        ag.drawTexture(self.fxTex, self.lightTex, int(self.player.centerX - self.cam.x - self.hs), int(self.player.centerY - self.cam.y - self.hs), replace=True)
        ag.drawTexture(self.tex, self.fxTex, 0, 0)

        ag.waitFPS()
        ag.updateWindow()
        ag.setWindowTitle(f"{self.title} (FPS: {ag.getFPS()})")

    def toggleFullscreen(self):
        self.fullscreen = not self.fullscreen
        ag.setWindowFullscreen(self.fullscreen)
        ag.setMouseVisible(not self.fullscreen)

    def quit(self):
        self.__running = False

def main():
    Game()
    ag.quit()

if __name__ == "__main__": main()
